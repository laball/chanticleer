<?php require_once("layouts/header.php");?>
	
	<section id="main"><!-- #main content and sidebar area -->
			<section id="content">
				<h1>Dining Experience Feedback</h1>
			<form id="feedbackForm">
          <fieldset>
						<label for="contactName">Your Name</label>
						<input name="contactName" id="contactName" type="text" class="input" />
						<span id="contactName_error" class="error">Please enter your name.</span>
					</fieldset> 
          <fieldset>
						<label for="contactEmail">Your Email</label>
						<input name="contactEmail" id="contactEmail" type="email" class="input" />
						<span class="error" id="contactEmail_error">Please enter a valid email address.</span>
					</fieldset>
          <fieldset>
						<label for="contactPhone">Your Phone Number</label>
						<input name="contactPhone" id="contactPhone" type="tel" class="input" />
					</fieldset>
          <fieldset>
						<label for="dateVisit">Date of Visit</label>
						<input name="dateVisit" id="dateVisit" type="text" class="input" />
						<!-- <span class="error" id="dateVisit_error">Please enter the date of your visit.</span> -->
					</fieldset>
          <fieldset>
						<label for="serverName">Name of Your Server</label>
						<input name="serverName" id="serverName" type="text" class="input" />
					</fieldset>
          <fieldset>
						<label for="foodItem">What food item did you<br />enjoy most during your<br />visit?</label>
						<input name="foodItem" id="foodItem" type="text" class="input" />
					</fieldset>
					<div class="clear"></div>
					<fieldset id="food"><label class="ratings">Food</label>
						<input id="food_excellent" name="food" type="radio" value="Excellent" />	<label class="checkbox" for="food_excellent">Excellent</label>
						<input id="food_good" name="food" type="radio" value="Good"  /><label class="checkbox" for="food_good">Good</label>
						<input id="food_fair" name="food" type="radio" value="Fair" /><label class="checkbox" for="food_fair">Fair</label>    
						<input id="food_poor" name="food" type="radio" value="Poor"  /><label class="checkbox" for="food_poor">Poor</label>
					</fieldset>
					<div class="clear"></div>
					<fieldset id="drinks"><label class="ratings">Drinks</label>
						<input id="drinks_excellent" name="drinks" type="radio" value="Excellent" />	<label class="checkbox" for="drinks_excellent">Excellent</label>
						<input id="drinks_good" name="drinks" type="radio" value="Good"  /><label class="checkbox" for="drinks_good">Good</label>
						<input id="drinks_fair" name="drinks" type="radio" value="Fair" /><label class="checkbox" for="drinks_fair">Fair</label>    
						<input id="drinks_poor" name="drinks" type="radio" value="Poor"  /><label class="checkbox" for="drinks_poor">Poor</label>
					</fieldset>
					<div class="clear"></div>
					<fieldset id="service"><label class="ratings">Service</label>
						<input id="service_excellent" name="service" type="radio" value="Excellent" />	<label class="checkbox" for="service_excellent">Excellent</label>
						<input id="service_good" name="service" type="radio" value="Good"  /><label class="checkbox" for="service_good">Good</label>
						<input id="service_fair" name="service" type="radio" value="Fair" /><label class="checkbox" for="service_fair">Fair</label>    
						<input id="service_poor" name="service" type="radio" value="Poor"  /><label class="checkbox" for="service_poor">Poor</label>
					</fieldset>
					<div class="clear"></div>
					<fieldset id="value"><label class="ratings">Value</label>
						<input id="value_excellent" name="value" type="radio" value="Excellent" />	<label class="checkbox" for="value_excellent">Excellent</label>
						<input id="value_good" name="value" type="radio" value="Good"  /><label class="checkbox" for="value_good">Good</label>
						<input id="value_fair" name="value" type="radio" value="Fair" /><label class="checkbox" for="value_fair">Fair</label>    
						<input id="value_poor" name="value" type="radio" value="Poor"  /><label class="checkbox" for="value_poor">Poor</label>
					</fieldset>
					<div class="clear"></div>
					<fieldset id="atmosphere"><label class="ratings">Atmosphere</label>
						<input id="atmosphere_excellent" name="atmosphere" type="radio" value="Excellent" />	<label class="checkbox" for="atmosphere_excellent">Excellent</label>
						<input id="atmosphere_good" name="atmosphere" type="radio" value="Good"  /><label class="checkbox" for="atmosphere_good">Good</label>
						<input id="atmosphere_fair" name="atmosphere" type="radio" value="Fair" /><label class="checkbox" for="atmosphere_fair">Fair</label>    
						<input id="atmosphere_poor" name="atmosphere" type="radio" value="Poor"  /><label class="checkbox" for="atmosphere_poor">Poor</label>
					</fieldset>
					<div class="clear"></div>
          <fieldset class="message">
					<label id="msgLabel" for="contactMessage"><img src="images/MessageHeading.png"></label>
						<textarea rows="5" cols="10" name="contactMessage" id="contactMessage"></textarea>
					</fieldset>
					<img id="loader" src="images/ajax-loader.gif" alt="Loading" />
					<div id="message">
						<img id="rooster" src="images/rooster-message.png">
						<p id="form-message"></p>
					</div>
          <fieldset>
						<input type="hidden" name="bot" id="bot" />
						<input type="button" name="send" id="send"  />
					</fieldset>
        </form>
			</section>
			<div id="scrollAnchor"></div>

<?php require_once("layouts/sidebar.php");?>
	</section><!-- end of #main content and sidebar-->
	<?php require_once("layouts/footer.php");?>
