(function($){								                       //function to create private scope with $ parameter
	var cssObj = {"background-color":"transparent", "border-bottom-style":"dashed", "border-bottom-color":"#952225", "border-bottom-width":"2px"};
	var cssErrObj = {"background-color":"#FEC765"};
	
	$(document).ready(function() { 

		function fnDelay(){
			$.scrollTo('max');
		}

	//external links to open in a new window
   $('a[rel="external"]').attr('target', '_blank').addClass('external');

//side menu triggering
		$(".trigger").click(function(){
			$(".panel").toggle("fast");
			$(this).toggleClass("active");
			return false;
		});

	//prettyPhoto lightbox
  $("a[rel^='prettyPhoto']").prettyPhoto({
    animationSpeed: 'normal', /* fast/slow/normal/milliseconds, unquoted */
    padding: 40, /* padding for each side of the picture */
    opacity: 0.75, 
    showTitle: true, 
    counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
    theme: 'dark_square' /* light_rounded / dark_rounded / light_square / dark_square */
  });

//hide and jQuerify UX components
$('.error').css({"visibility":"hidden"});
$('#form-message').hide();
$('#rooster').hide();
$('#food, #drinks, #service, #music,#value, #atmosphere').buttonset();
$('#dateVisit').datepicker();
$('#contactPhone').mask("(999) 999-9999");

//image gallery
	Galleria.loadTheme('js/plugins/themes/classic/galleria.classic.js');

		var data = [

	      {
	          image: 'images/gallery/12.jpg',
						title: 'Famous Onion Rings'
	      },
	      {
	          image: 'images/gallery/14.jpg',
						title: 'Sirloin Steak'
	      },
	      {
	          image: 'images/gallery/20.jpg',
						title: 'Tilapia'
	      },
	      {
	          image: 'images/gallery/29.jpg',
						title: 'Grilled Chicken Salad'
	      },
	      {
	          image: 'images/gallery/31.jpg',
						title: 'Tenderloin Sandwich'
	      },
	      {
	          image: 'images/gallery/36.jpg',
						title: 'Fried Chicken'
	      }
	  ];

	//shuffle array with Fisher-Yates algorithm
	  function fisherYates ( myArray ) {
		  var i = myArray.length;
		  if ( i == 0 ) return false;
		  while ( --i ) {
		     var j = Math.floor( Math.random() * ( i + 1 ) );
		     var tempi = myArray[i];
		     var tempj = myArray[j];
		     myArray[i] = tempj;
		     myArray[j] = tempi;
		   }
		}
		fisherYates(data);

		$('#gallery').galleria({
			data_source:data, 
			transition:'fade',
			transition_speed:1500,
			autoplay:5000,
			height:169,
			width:242,
			show_imagenav:false,
			thumbnails:false,
			extend: function(options) {
				        // listen to when an image is shown
				        this.bind(Galleria.IMAGE, function(e) {
				            // lets make galleria open a lightbox when clicking the main image:
				            $(e.imageTarget).click(this.proxy(function() {
				               this.openLightbox();
				            }));
				        });
				    }
			// debug:true
		});

//FORM validation
//setup error classes
		$.fn.errorStyle = function() {
		  this.replaceWith(function(i,html){
		      // var StyledError = "<div class=\"ui-state-error ui-corner-all\" style=\"position:relative;top:5px;right:-25px;\">";
		      var StyledError = "<div class=\"ui-state-error ui-corner-all\" style=\"float:right;\">";
		      StyledError += "<p style=\"margin-bottom:0;\"><span class=\"ui-icon ui-icon-alert\">";
		      StyledError += "</span><span class='error'>";
		      StyledError += html;
		      StyledError += "</span>";
		      StyledError += "</p></div>";
		      return StyledError;
		   });
		};
		//validation object
		var validateElement = {
			isValid:function(element){
				var isValid = true;
				$(element).css(cssObj);
				var $element = $(element);
				var name = $element.attr('name');
				var value = $element.val();
				var id = $element.attr('id');
				var error = "#" + id + "_error";
				// <input> uses type attribute as written in tag
				// <textarea> has intrinsic type of 'textarea'
				// <select> has intrinsic type of 'select-one' or 'select-multiple' 
				// <checkbox> has intrinsic type of 'checkbox'
				var type = $element[0].type.toLowerCase();
				switch(type){
					case 'text':
						if(name == "contactName" ){
						 	if(value.length == 0){
								isValid = false;
							}
						}
						if(name == "contactEmail"){
						 	var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
						 	isValid = pattern.test(value);
						}
						break;
				} //end switch
				if(!isValid){
					// var errorMsg = $(error).errorStyle();									
					// if(nameErrorMsg.length > 0){
						// var errMsg = $(nameErrorMsg).errorStyle();
						// $(errMsg).appendTo($element);	
						$(error).errorStyle();
						$(element).addClass('.input-error');
						$(element).css(cssErrObj);
					// } 
				}
				return isValid;
			} //end isValid method
		}; //end validateElement Object

		//FORM EVENTS OBJECT
		var feedback = {
			initEventHandlers: function(){
				$('#loader').hide();
				//FORM Submit
				$('#send').bind('click', function(event){
					var valid = true;
					$('#feedbackForm input[type=text], #feedbackForm input[type=email]').each(function(){
						if(!validateElement.isValid($(this))){
							$(this).css(cssErrObj);
							$(this).next('div').css({"visibility":"visible"});							
							valid = false;
						}
					});
					if(!valid){
						$.scrollTo('h1',{duration:2000});
						event.preventDefault();
						return false;
						$('#loader').hide();
					}else{
						$('.ui-state-error .ui-corner-all').css({"visibility":"hidden"});
						$('#loader').show();
						// setTimeout('feedback.feedbackFormSubmit()',500);							
						feedback.feedbackFormSubmit();
					}
				});
				//FORM behaviors
				$('#contactName').bind('blur', function(){
						if(!validateElement.isValid($(this))){
							$(this).next('div').css({"visibility":"visible"});
							$(this).css(cssErrObj);							
						}
				});
				$('#contactEmail').bind('blur', function(){
					if(!validateElement.isValid($(this))){
						$(this).next('div').css({"visibility":"visible"});							
						$(this).css(cssErrObj);
					}
				});
				$('#feedbackForm input[type=text], #feedbackForm input[type=email]').bind('keydown', function(event){ 
					var keyCode = event.which;
					var excludedKeyCodes = ",9,13,37,38,39,40,";
					var excluded = (excludedKeyCodes.indexOf(',' + keyCode + ',') > -1);
					if (keyCode == 13){  
						$(this).next('div').css({"visibility":"hidden"});
						$(this).css(cssObj);
						$('#send').click();
						return false;
					}
					if(!excluded){
						$(this).next('div').css({"visibility":"hidden"});
						$(this).css(cssObj);
					}
				});
			},
			feedbackFormSubmit: function(){
				$.ajax({
					   type: 'POST',
					   url: 'scripts/feedback.php',
					   dataType: 'json',
					   data	: $('#feedbackForm').serialize(),
					   success: function(data,textStatus){
									  //hide the ajax loader
									  $('#loader').hide();
									  if(data.result == '1'){
									      //show success message
											$('#rooster').show('fast');
										  $('#form-message').hide().css({"color":"#5C495B"}).empty().html(data.message).show("fast");
											setTimeout(fnDelay, 500);
										  //reset all form fields
									  }else{
											if(data.result == '-1'){
												$('#rooster').show('fast');
										  	$('#form-message').hide().css({"color":"#5C495B"}).empty().html(data.message).show("fast");
												setTimeout(fnDelay, 500);
						 					}
										}
						 },
					   error: function(data,textStatus){
					  	$('#loader').hide();
							$('#rooster').show('fast');
							$('#form-message').hide().css({"color":"#5C495B"}).empty().html("There was a problem: " + textStatus).show("fast");
							setTimeout(fnDelay, 500);
						 }
				});
			}  
		};
		feedback.initEventHandlers();
		
		vtip();
//splash screen
	// $('#target').meerkat({
	// 	background: '#D7B693 url(images/29bg.jpg) repeat',
	// 	height: '100%',
	// 	width: '100%',
	// 	position: 'top',
	// 	dontShowAgain: '#target',
	// 	// animationIn: 'none',
	// 	animationOut: 'fade',
	// 	animationSpeed: 1500,
	// 	timer: 2,
	// 	opacity:0.85,
	// 	removeCookie: '.reset'
	// 	// easingIn:'easeInBounce',
	// 	// easingOut:'easeOutElastic'
	// });

	}); 				//document.ready
})(jQuery);  //invoke nameless function that creates the private scope, and pass it the global reference to the jQuery object