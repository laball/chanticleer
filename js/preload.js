(function($){								                       //function to create private scope with $ parameter
	$(document).ready(function() { 

		// $('html').css({opacity:'0.85'});
		//preload CSS images
		$.preloadCssImages();
		//preload images
		function preload(imageArray){
			$(imageArray).each(function(){
				$('<img/>')[0].src = this;
				});
			}
			preload([
				'images/Chanticleer-Rooster.jpg',
				'images/image1.jpg',
				'images/image2.jpg',
				'images/home.png',
				'images/food.png',
				'images/wine.png',
				'images/clock.png',
				'images/checkbox.png',
				'images/megaphone.png',
				'images/home-white.png',
				'images/food-white.png',
				'images/wine-white.png',
				'images/clock-white.png',
				'images/checkbox-white.png',
				'images/megaphone-white.png',
				'images/image2.jpg',
				'images/facebook.png',
				'images/twitter.png']);
	
	}); 				//document.ready
})(jQuery);  //invoke nameless function that creates the private scope, and pass it the global reference to the jQuery object