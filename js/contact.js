(function($){								                       //function to create private scope with $ parameter
	var cssObj = {"background-color":"transparent", "border-bottom-style":"dashed", "border-bottom-color":"#952225", "border-bottom-width":"2px"};
	var cssErrObj = {"background-color":"#FEC765"};
	$(document).ready(function() { 

    //hide facebook widget since site isn't live anymore
    $('a.trigger').hide();

		function fnDelay(){
			$.scrollTo('#scrollAnchor');
		}

		//external links to open in a new window
    $('a[rel="external"]').attr('target', '_blank').addClass('external');

		//side menu triggering

				$(".trigger").click(function(){
					$(".panel").toggle("fast");
					$(this).toggleClass("active");
					return false;
				});


	//prettyPhoto lightbox
  $("a[rel^='prettyPhoto']").prettyPhoto({
    animationSpeed: 'normal', /* fast/slow/normal/milliseconds, unquoted */
    padding: 40, /* padding for each side of the picture */
    opacity: 0.75, 
    showTitle: true, 
    counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
    theme: 'dark_square' /* light_rounded / dark_rounded / light_square / dark_square */
  });


//hide and jQuerify UX components
$('.error').css({"visibility":"hidden"});
$('#form-message').hide();
$('#rooster').hide();

//FORM validation
//setup error classes
		$.fn.errorStyle = function() {
		  this.replaceWith(function(i,html){
		      // var StyledError = "<div class=\"ui-state-error ui-corner-all\" style=\"position:relative;top:5px;right:-25px;\">";
		      var StyledError = "<div class=\"ui-state-error ui-corner-all\" style=\"float:right;\">";
		      StyledError += "<p style=\"margin-bottom:0;\"><span class=\"ui-icon ui-icon-alert\">";
		      StyledError += "</span><span class='error'>";
		      StyledError += html;
		      StyledError += "</span>";
		      StyledError += "</p></div>";
		      return StyledError;
		   });
		};
		//validation object
		var validateElement = {
			isValid:function(element){
				var isValid = true;
				$(element).css(cssObj);
				var $element = $(element);
				var name = $element.attr('name');
				var value = $element.val();
				var id = $element.attr('id');
				var error = "#" + id + "_error";
				// <input> uses type attribute as written in tag
				// <textarea> has intrinsic type of 'textarea'
				// <select> has intrinsic type of 'select-one' or 'select-multiple' 
				// <checkbox> has intrinsic type of 'checkbox'
				var type = $element[0].type.toLowerCase();
				switch(type){
					case 'text':
						if(name == "contactName" ){
						 	if(value.length == 0){
								isValid = false;
							}
						}
						if(name == "contactEmail"){
						 	var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
						 	isValid = pattern.test(value);
						}
						break;
				} //end switch
				if(!isValid){
					// var errorMsg = $(error).errorStyle();									
					// if(nameErrorMsg.length > 0){
						// var errMsg = $(nameErrorMsg).errorStyle();
						// $(errMsg).appendTo($element);	
						$(error).errorStyle();
						$(element).addClass('.input-error');
						$(element).css(cssErrObj);
					// } 
				}
				return isValid;
			} //end isValid method
		}; //end validateElement Object

		//FORM EVENTS OBJECT
		var contact = {
			initEventHandlers: function(){
				$('#loader').hide();
				//FORM Submit
				$('#send').bind('click', function(event){
					var valid = true;
					$('#contactForm input[type=text], #contactForm input[type=email]').each(function(){
						if(!validateElement.isValid($(this))){
							$(this).css(cssErrObj);
							$(this).next('div').css({"visibility":"visible"});							
							valid = false;
						}
					});
					if(!valid){
						$.scrollTo('h1',{duration:2000});
						event.preventDefault();
						return false;
						$('#loader').hide();
					}else{
						$('.ui-state-error .ui-corner-all').css({"visibility":"hidden"});
						$('#loader').show();
						// setTimeout('feedback.contactFormSubmit()',500);							
						contact.contactFormSubmit();
					}
				});
				//FORM behaviors
				$('#contactName, #contactEmail').bind('blur', function(){
						if(!validateElement.isValid($(this))){
							$(this).next('div').css({"visibility":"visible"});
							$(this).css(cssErrObj);							
						}
				});
        // $('#contactEmail').bind('blur', function(){
        //  if(!validateElement.isValid($(this))){
        //    $(this).next('div').css({"visibility":"visible"});              
        //    $(this).css(cssErrObj);
        //  }
        // });
				$('#contactForm input[type=text]').bind('keydown', function(event){ 
					var keyCode = event.which;
					var excludedKeyCodes = ",9,13,37,38,39,40,";
					var excluded = (excludedKeyCodes.indexOf(',' + keyCode + ',') > -1);
					if (keyCode == 13){  
						$(this).next('div').css({"visibility":"hidden"});
						$(this).css(cssObj);
						$('#send').click();
						return false;
					}
					if(!excluded){
						$(this).next('div').css({"visibility":"hidden"});
						$(this).css(cssObj);
					}
				});
			},
			contactFormSubmit: function(){
				$.ajax({
					   type: 'POST',
					   url: 'scripts/contact.php',
					   dataType: 'json',
					   data	: $('#contactForm').serialize(),
					   success: function(data,textStatus){
									  //hide the ajax loader
									  $('#loader').hide();
									  if(data.result == '1'){
									      //show success message
												$('#rooster').show('fast');
											  $('#form-message').hide().css({"color":"#5C495B"}).empty().html(data.message).show("fast");
										  //reset all form fields
									  }else{
											if(data.result == '-1'){
												$('#rooster').show('fast');
										  	$('#form-message').hide().css({"color":"#5C495B"}).empty().html(data.message).show("fast");
												setTimeout(fnDelay, 500);
						 					}
										}
						 },
					   error: function(data,textStatus){
					  	$('#loader').hide();
							$('#rooster').show('fast');
							$('#form-message').hide().css({"color":"#5C495B"}).empty().html("There was a problem: " + textStatus).show("fast");
							setTimeout(fnDelay, 500);
						 }
				});
			}  
		};
		contact.initEventHandlers();
//splash screen
	// $('#target').meerkat({
	// 	background: '#D7B693 url(images/29bg.jpg) repeat',
	// 	height: '100%',
	// 	width: '100%',
	// 	position: 'top',
	// 	dontShowAgain: '#target',
	// 	// animationIn: 'none',
	// 	animationOut: 'fade',
	// 	animationSpeed: 1500,
	// 	timer: 2,
	// 	opacity:0.85,
	// 	removeCookie: '.reset'
	// 	// easingIn:'easeInBounce',
	// 	// easingOut:'easeOutElastic'
	// });

	}); 				//document.ready
})(jQuery);  //invoke nameless function that creates the private scope, and pass it the global reference to the jQuery object