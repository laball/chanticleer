<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ --> 
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<title>Hodel&rsquo;s Chanticleer &bull; Eureka, IL</title>
	<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/jquery-ui-1.8.10.custom.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
	<link rel="shortcut icon" href="favicon.ico">
	<script src="js/lib/modernizr-1.6.min.js"></script>
	<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
	<script>!window.jQuery && document.write('<script src="js/lib/jquery-1.4.4.min.js"><\/script>')</script>
	<script src="js/lib/jquery-ui-1.8.10.custom.min.js"></script>
	<script src="js/lib/dlink.js"></script>
	<script src="js/preload.js"></script>	
	<script src="js/plugins/jquery.maskedinput-1.2.2.min.js"></script>
	<script src="js/plugins/jquery.scrollTo-min.js"></script>
	<script src="js/plugins/jquery.prettyPhoto.js"></script>
	<script src="js/plugins/galleria.js"></script>
	<script src="js/plugins/jquery.meerkat.1.3.min.js"></script>
	<script src="js/plugins/preloadCssImages.jQuery_v5.js"></script>
	<!--[if (gt IE 7)|!(IE)]><!-->
	<script src="js/plugins/vtip.js"></script>
	<!--<![endif]-->
<?php if($_SERVER["PHP_SELF"] == "/Chanticleer/contact.php"){?>
	<script src="js/contact.js"></script>	
<?php }else{?>
	<script src="js/script.js"></script>	
<?php } ?>
	<!--[if lt IE 7 ]>
	  <script src="assets/js/lib/dd_belatedpng.js?v=1"></script>
		<script> DD_belatedPNG.fix('img, .png_bg'); //fix any <img> or .png_bg background-images </script>
	<![endif]-->
	<script>//http://24ways.org/2010/using-the-webfont-loader-to-make-browsers-behave-the-same	
		WebFontConfig = {
			custom: { families: ['ChanticleerRomanRegular','GoodDogRegular'],
			urls: [ 'http://hodelschanticleer.com/css/styles.css' ] }
		};
		(function(){
	// Paul Irish - no font flicker http://www.position-absolute.com/articles/introduction-to-the-google-webfont-loader-and-how-to-avoid-font-face-text-flickering-with-it/
	// document.getElementsByTagName("html")[0].setAttribute("class","wf-loading");
	  //  NEEDED to push the wf-loading class to your head
	  // document.getElementsByTagName("html")[0].setAttribute("className","wf-loading");
	        // for IE…
					$('html').addClass("wf-loading");

	         var wf = document.createElement('script');
	         wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
	             '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	         wf.type = 'text/javascript';
	         wf.async = 'false';
	         var s = document.getElementsByTagName('script')[0];
	         s.parentNode.insertBefore(wf, s);
	
		})();
	</script>
</head>
<body>
	<!-- <div id="target">
		<img id="splash" src="images/Chanticleer-Rooster.jpg" />
	</div> -->
<div id="wrapper"><!-- #wrapper -->
	<nav><!-- top nav -->
		<?php $serverPath = $_SERVER["PHP_SELF"];
					$path="/";
		?>
			<ul>
				<?php 
					$page = $path . "index.php";
					if($serverPath == $page){
						echo "<li><a href=\"#\" class=\"home selected\"><img src=\"images/home.png\" /> Home</a></li>";
					}else{
						echo "<li><a class=\"home\" href=\"index.php\"><img src=\"images/home.png\" /> Home</a></li>";						
					}
					$page = $path . "food.php";
					if($serverPath == $page){
						echo "<li><a href=\"#\" class=\"food selected\"><img src=\"images/food.png\" /> Food</a></li>";
					}else{
						echo "<li><a class=\"food\" href=\"Menu.pdf?iframe=true&amp;width=100%&amp;height=100%\" rel=\"prettyPhoto\"><img src=\"images/food.png\" /> Food</a></li>";						
					}
					$page = $path . "wine-beer.php";
					if($serverPath == $page){
						echo "<li><a href=\"#\" class=\"wine selected\"><img src=\"images/wine.png\" /> Beer &amp; Wine</a></li>";
					}else{
						echo "<li><a class=\"wine\" href=\"Drinks.pdf?iframe=true&amp;width=100%&amp;height=100%\" rel=\"prettyPhoto\"><img src=\"images/wine.png\" /> Drinks</a></li>";						
					}
					$page = $path . "reservations.php";
					if($serverPath == $page){
						echo "<li><a href=\"#\" class=\"reservations selected\"><img src=\"images/clock.png\" /> Reservations</a></li>";
					}else{
						echo "<li><a class=\"reservations\" href=\"reservations.php\"><img src=\"images/clock.png\" /> Reservations</a></li>";						
					}
					$page = $path . "feedback.php";
					if($serverPath == $page){
						echo "<li><a href=\"#\" class=\"feedback selected\"><img src=\"images/checkbox.png\" /> Feedback</a></li>";
					}else{
						echo "<li><a class=\"feedback\" href=\"feedback.php\"><img src=\"images/checkbox.png\" /> Feedback</a></li>";						
					}
					$page = $path . "contact.php";
					if($serverPath == $page){
						echo "<li><a href=\"#\" class=\"contact selected\"><img src=\"images/megaphone.png\" /> Contact</a></li>";
					}else{
						echo "<li><a class=\"contact\" href=\"contact.php\"><img src=\"images/megaphone.png\" /> Contact</a></li>";						
					}
				?>

				<!-- <li><a class="home selected" href="#"><img src="images/home.png" alt="" />Home</a></li>
				<li><a class="food" href="food.html"><img src="images/food.png" alt="" />Food</a></li>
				<li><a class="wine" href="wine-beer.html"><img src="images/wine.png" alt="" />Beer &amp; Wine</a></li>
				<li><a class="reservations" href="reservations.html"><img src="images/clock.png" alt="" />Reservations</a></li>
				<li><a class="feedback" href="feedback.html"><img src="images/checkbox.png" alt="" />Feedback</a></li>
				<li><a class="contact" href="contact.html"><img src="images/megaphone.png" alt="" />Contact</a></li> -->
			</ul>
	</nav><!-- end of top nav -->

	<header><!-- header -->
		<h1><a href="#">Hodel&rsquo;s Chanticleer</a></h1>
		<div class="vcard">
			<div class="addr">
				<span class="type">Work</span>
				<span class="street-address"><span class="number-raise">744</span> North Main Street</span>
				<span class="locality">Eureka</span>, 
				<abbr class="region" title="Illinois">IL</abbr>&nbsp;
				<span class="postal-code"><span class="zip-nbr-raise">61530</span></span>
				<div class="country-name">USA</div>
			</div>
			<div class="tel">
				   <span class="type">Work</span> <span class="number-raise">309-467-4934</span>
				  </div>
				  <div> 
				   <span class="email">info@hodelschanticleer.com</span>
				  </div>
		</div>
	</header><!-- end of header -->
	
	
