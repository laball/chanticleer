<?php require_once("layouts/header.php");?>
	<section id="main"><!-- #main content and sidebar area -->
			<section id="content"><!-- #content -->
			
					<h1>Contact Us</h1>
				<form id="contactForm">
	          <fieldset>
							<label for="contactName">Your Name</label>
							<input name="contactName" id="contactName" type="text" class="input" />
							<span id="contactName_error" class="error">Please enter your name.</span>
						</fieldset> 
	          <fieldset>
							<label for="contactEmail">Your Email</label>
							<input name="contactEmail" id="contactEmail" type="text" class="input" />
							<span class="error" id="contactEmail_error">Please enter a valid email address.</span>
						</fieldset>
							<div class="clear"></div>
		          <fieldset class="message">
							<label id="msgLabel" for="contactMessage"><img src="images/MessageHeading.png"></label>
								<textarea rows="5" cols="10" name="contactMessage" id="contactMessage"></textarea>
							</fieldset>
							<img id="loader" src="images/ajax-loader.gif" alt="Loading" />
							<div id="message">
								<img id="rooster" src="images/rooster-message.png">
								<p id="form-message"></p>
							</div>
		          <fieldset>
								<input type="hidden" name="bot" id="bot" />
								<input type="button" name="send" id="send"  />
							</fieldset>
		        </form>						
			</section><!-- end of #content -->
			<div id="scrollAnchor"></div>
			<?php require_once("layouts/sidebar.php");?>
		</section> <!-- end of #main -->
	<?php require_once("layouts/footer.php");?>
