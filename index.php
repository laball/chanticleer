<?php require_once("layouts/header.php");?>
	<section id="main"><!-- #main content and sidebar area -->
			<section id="content"><!-- #content -->
			
        		<article>
								<h2><a href="#">About Us</a></h2>
								<img src="images/Chanticleer2.jpg" alt="Chanticleer Illustration" class="alignleft box-shadow" /><p>Located in Eureka IL, Hodel&rsquo;s Chanticleer is a charming neighborhood restaurant known for it&rsquo;s famous fried chicken and onion rings. If fried chicken is not for you then choose from one of our many menu items. Any one of which is sure to satisfy. Hodel&rsquo;s Chanticleer has a family friendly dining experience that offers quality, affordable meals. If you are looking for a great place to hold your next party or meeting, we also offer a separate dining area capable of holding groups up to 60. Or if you are just looking to relax with friends, we also have a full service bar to accommodate your needs.</p>
							</article>
	        		<article>
								<h2><a href="#">Our History</a></h2>
								<p><img src="images/Chanticleer1.jpg" alt="Chanticleer Illustration" class="alignright box-shadow" />The Chanticleer has been serving the central Illinois area for over 50 years. We take pride in serving Eureka and the surrounding area. Owner since 2000, Steve Hodel welcomes you to come on in and try us out. Steve takes pride in his business and has shown that with his efforts to remodel the restaurant in order to make a more pleasant dining experience for the customer. We thank all of our customers for their continued support and look forward to seeing them (and perhaps a few new faces) soon.</p>
							</article>
							<img class="vtip box-shadow" src="images/Bar.jpg" alt="Bar Area" />
							<img class="vtip box-shadow" src="images/DiningArea.jpg" alt="Dining Area" />
							<img class="vtip box-shadow" src="images/DiningArea2.jpg" alt="Banquet Dining Area" />
			</section><!-- end of #content -->

			<?php require_once("layouts/sidebar.php");?>

	</section><!-- end of #main content and sidebar-->

<?php require_once("layouts/footer.php");?>
